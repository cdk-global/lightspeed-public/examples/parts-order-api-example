# Example Lightspeed General Purchase Order API

# Getting Started

This is an example RESTful API using Spring Boot that 3rd parties can use to get started when making an integration with
Lightspeed to accept exported purchase orders. A Lightspeed dealer will be able to export purchase orders to a 3rd party as long 
as the 3rd party's service is 
1. publicly accessible, 
2. has Basic Auth, 
3. can accept RESTful requests that adhere to the API defined in PartsOrderController.java, 
4. uses HTTPS, and 
5. is configured correctly by a Lightspeed dealer.

While the RESTful API of this example Spring Boot service is fully defined and adheres to Lightspeed's specifications, the actual
work to save and query the data is not implemented. Please refer to each TODO in the code to see what remains to be done.

Down below is some additional documentation on Spring Boot and the technologies used in this example API.

### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.2/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.2/gradle-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.4.2/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring Security](https://docs.spring.io/spring-boot/docs/2.4.2/reference/htmlsingle/#boot-features-security)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Securing a Web Application](https://spring.io/guides/gs/securing-web/)
* [Spring Boot and OAuth2](https://spring.io/guides/tutorials/spring-boot-oauth2/)
* [Authenticating a User with LDAP](https://spring.io/guides/gs/authenticating-ldap/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

