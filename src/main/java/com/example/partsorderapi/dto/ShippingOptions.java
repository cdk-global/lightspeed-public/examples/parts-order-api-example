package com.example.partsorderapi.dto;

import java.util.ArrayList;
import java.util.List;

public class ShippingOptions {

    private List<String> shippingOptions = new ArrayList<>();

    public List<String> getShippingOptions() {
        return shippingOptions;
    }

    public void setShippingOptions(List<String> shippingOptions) {
        this.shippingOptions = shippingOptions;
    }
}
