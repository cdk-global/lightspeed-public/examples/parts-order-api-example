package com.example.partsorderapi.dto;

import java.math.BigDecimal;

public class PurchaseOrderLine {

    private String supplierPurchaseOrderLineID;
    private String partNumber;
    private String partDescription;
    private BigDecimal dealershipCost;
    private BigDecimal orderQuantity;
    private String supplierStatus;
    private String notes;
    private Integer specialOrderNumber;
    private Integer repairOrderNumber;
    private String majorUnitStockNumber;
    private String majorUnitVinNumber;

    public String getSupplierPurchaseOrderLineID() {
        return supplierPurchaseOrderLineID;
    }

    public void setSupplierPurchaseOrderLineID(String supplierPurchaseOrderLineID) {
        this.supplierPurchaseOrderLineID = supplierPurchaseOrderLineID;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getPartDescription() {
        return partDescription;
    }

    public void setPartDescription(String partDescription) {
        this.partDescription = partDescription;
    }

    public BigDecimal getDealershipCost() {
        return dealershipCost;
    }

    public void setDealershipCost(BigDecimal dealershipCost) {
        this.dealershipCost = dealershipCost;
    }

    public BigDecimal getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(BigDecimal orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public String getSupplierStatus() {
        return supplierStatus;
    }

    public void setSupplierStatus(String supplierStatus) {
        this.supplierStatus = supplierStatus;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getSpecialOrderNumber() {
        return specialOrderNumber;
    }

    public void setSpecialOrderNumber(Integer specialOrderNumber) {
        this.specialOrderNumber = specialOrderNumber;
    }

    public Integer getRepairOrderNumber() {
        return repairOrderNumber;
    }

    public void setRepairOrderNumber(Integer repairOrderNumber) {
        this.repairOrderNumber = repairOrderNumber;
    }

    public String getMajorUnitStockNumber() {
        return majorUnitStockNumber;
    }

    public void setMajorUnitStockNumber(String majorUnitStockNumber) {
        this.majorUnitStockNumber = majorUnitStockNumber;
    }

    public String getMajorUnitVinNumber() {
        return majorUnitVinNumber;
    }

    public void setMajorUnitVinNumber(String majorUnitVinNumber) {
        this.majorUnitVinNumber = majorUnitVinNumber;
    }
}
