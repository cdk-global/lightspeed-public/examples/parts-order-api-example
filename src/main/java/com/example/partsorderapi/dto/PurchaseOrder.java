package com.example.partsorderapi.dto;

import java.util.ArrayList;
import java.util.List;

public class PurchaseOrder {

    private String supplierPurchaseOrderNumber;
    private String supplierPurchaseOrderID;
    private String lightSpeedDealerNumber;
    private String lightSpeedPurchaseOrderNumber;
    private String orderType;
    private String orderNotes;
    private String createdDate;
    private String shippingOption;
    private String shippingInstructions;
    private String shipToName;
    private String shipToAddress;
    private String shipToCity;
    private String shipToProvince;
    private String shipToPostalCode;
    private String shipToCountry;
    private String billToName;
    private String billToAddress;
    private String billToCity;
    private String billToProvince;
    private String billToPostalCode;
    private String billToCountry;
    private List<PurchaseOrderLine> lines = new ArrayList<>();

    public String getSupplierPurchaseOrderNumber() {
        return supplierPurchaseOrderNumber;
    }

    public void setSupplierPurchaseOrderNumber(String supplierPurchaseOrderNumber) {
        this.supplierPurchaseOrderNumber = supplierPurchaseOrderNumber;
    }

    public String getSupplierPurchaseOrderID() {
        return supplierPurchaseOrderID;
    }

    public void setSupplierPurchaseOrderID(String supplierPurchaseOrderID) {
        this.supplierPurchaseOrderID = supplierPurchaseOrderID;
    }

    public String getLightSpeedDealerNumber() {
        return lightSpeedDealerNumber;
    }

    public void setLightSpeedDealerNumber(String lightSpeedDealerNumber) {
        this.lightSpeedDealerNumber = lightSpeedDealerNumber;
    }

    public String getLightSpeedPurchaseOrderNumber() {
        return lightSpeedPurchaseOrderNumber;
    }

    public void setLightSpeedPurchaseOrderNumber(String lightSpeedPurchaseOrderNumber) {
        this.lightSpeedPurchaseOrderNumber = lightSpeedPurchaseOrderNumber;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderNotes() {
        return orderNotes;
    }

    public void setOrderNotes(String orderNotes) {
        this.orderNotes = orderNotes;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getShippingOption() {
        return shippingOption;
    }

    public void setShippingOption(String shippingOption) {
        this.shippingOption = shippingOption;
    }

    public String getShippingInstructions() {
        return shippingInstructions;
    }

    public void setShippingInstructions(String shippingInstructions) {
        this.shippingInstructions = shippingInstructions;
    }

    public String getShipToName() {
        return shipToName;
    }

    public void setShipToName(String shipToName) {
        this.shipToName = shipToName;
    }

    public String getShipToAddress() {
        return shipToAddress;
    }

    public void setShipToAddress(String shipToAddress) {
        this.shipToAddress = shipToAddress;
    }

    public String getShipToCity() {
        return shipToCity;
    }

    public void setShipToCity(String shipToCity) {
        this.shipToCity = shipToCity;
    }

    public String getShipToProvince() {
        return shipToProvince;
    }

    public void setShipToProvince(String shipToProvince) {
        this.shipToProvince = shipToProvince;
    }

    public String getShipToPostalCode() {
        return shipToPostalCode;
    }

    public void setShipToPostalCode(String shipToPostalCode) {
        this.shipToPostalCode = shipToPostalCode;
    }

    public String getShipToCountry() {
        return shipToCountry;
    }

    public void setShipToCountry(String shipToCountry) {
        this.shipToCountry = shipToCountry;
    }

    public String getBillToName() {
        return billToName;
    }

    public void setBillToName(String billToName) {
        this.billToName = billToName;
    }

    public String getBillToAddress() {
        return billToAddress;
    }

    public void setBillToAddress(String billToAddress) {
        this.billToAddress = billToAddress;
    }

    public String getBillToCity() {
        return billToCity;
    }

    public void setBillToCity(String billToCity) {
        this.billToCity = billToCity;
    }

    public String getBillToProvince() {
        return billToProvince;
    }

    public void setBillToProvince(String billToProvince) {
        this.billToProvince = billToProvince;
    }

    public String getBillToPostalCode() {
        return billToPostalCode;
    }

    public void setBillToPostalCode(String billToPostalCode) {
        this.billToPostalCode = billToPostalCode;
    }

    public String getBillToCountry() {
        return billToCountry;
    }

    public void setBillToCountry(String billToCountry) {
        this.billToCountry = billToCountry;
    }

    public List<PurchaseOrderLine> getLines() {
        return lines;
    }

    public void setLines(List<PurchaseOrderLine> lines) {
        this.lines = lines;
    }
}
