package com.example.partsorderapi.dto;

import java.math.BigDecimal;

public class PurchaseOrderSearch {

    private String supplierPurchaseOrderNumber;
    private String supplierPurchaseOrderID;
    private String orderType;
    private String shippingOption;
    private String orderStatus;
    private BigDecimal orderTotal;
    private int partCount;
    private String createdDate;
    private String receivedDate;

    public String getSupplierPurchaseOrderNumber() {
        return supplierPurchaseOrderNumber;
    }

    public void setSupplierPurchaseOrderNumber(String supplierPurchaseOrderNumber) {
        this.supplierPurchaseOrderNumber = supplierPurchaseOrderNumber;
    }

    public String getSupplierPurchaseOrderID() {
        return supplierPurchaseOrderID;
    }

    public void setSupplierPurchaseOrderID(String supplierPurchaseOrderID) {
        this.supplierPurchaseOrderID = supplierPurchaseOrderID;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getShippingOption() {
        return shippingOption;
    }

    public void setShippingOption(String shippingOption) {
        this.shippingOption = shippingOption;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public BigDecimal getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(BigDecimal orderTotal) {
        this.orderTotal = orderTotal;
    }

    public int getPartCount() {
        return partCount;
    }

    public void setPartCount(int partCount) {
        this.partCount = partCount;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(String receivedDate) {
        this.receivedDate = receivedDate;
    }
}
