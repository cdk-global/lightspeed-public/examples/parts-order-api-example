package com.example.partsorderapi.controller;

import com.example.partsorderapi.dto.PurchaseOrder;
import com.example.partsorderapi.dto.PurchaseOrdersSearchResult;
import com.example.partsorderapi.dto.ShippingOptions;
import com.example.partsorderapi.service.PartsOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
public class PartsOrderController {

    private final PartsOrderService purchaseOrderService;

    @Autowired
    public PartsOrderController(PartsOrderService purchaseOrderService) {
        this.purchaseOrderService = purchaseOrderService;
    }

    @GetMapping("/test")
    public ResponseEntity<String> getTestStatus() {
        return new ResponseEntity<>("Up and running", HttpStatus.OK);
    }

    @PostMapping("/dealer/{supplierDealerNumber}/purchaseorder")
    public ResponseEntity<PurchaseOrder> importPurchaseOrder(@PathVariable String supplierDealerNumber, @RequestBody PurchaseOrder purchaseOrder) {
        return new ResponseEntity<>(purchaseOrderService.importPurchaseOrder(supplierDealerNumber, purchaseOrder), HttpStatus.CREATED);
    }

    @GetMapping("/dealer/{supplierDealerNumber}/purchaseorder/{purchaseOrderID}")
    public ResponseEntity<PurchaseOrder> getPurchaseOrder(@PathVariable String supplierDealerNumber, @PathVariable String purchaseOrderID) {
        return new ResponseEntity<>(purchaseOrderService.getPurchaseOrder(supplierDealerNumber, purchaseOrderID), HttpStatus.OK);
    }

    @GetMapping("/dealer/{supplierDealerNumber}/purchaseorder/search")
    public ResponseEntity<PurchaseOrdersSearchResult> searchPurchaseOrders(@PathVariable String supplierDealerNumber, @RequestParam(value = "startDate") String startDateString, @RequestParam(value = "endDate") String endDateString) {
        //Return status code 501 if this endpoint is not implemented
        //return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

        //Dates will come in the "yyyy-MM-dd" format. Ex: 2020-10-31
        LocalDate startDate = LocalDate.parse(startDateString);
        LocalDate endDate = LocalDate.parse(endDateString);
        return new ResponseEntity<>(purchaseOrderService.searchPurchaseOrders(supplierDealerNumber, startDate, endDate), HttpStatus.OK);
    }

    @GetMapping("/dealer/{supplierDealerNumber}/purchaseorder/shippingoptions")
    public ResponseEntity<ShippingOptions> getShippingOptions(@PathVariable String supplierDealerNumber) {
        //Return status code 501 if this endpoint is not implemented
        //return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

        return new ResponseEntity<>(purchaseOrderService.getShippingOptions(supplierDealerNumber), HttpStatus.OK);
    }
}
