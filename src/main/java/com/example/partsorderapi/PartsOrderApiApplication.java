package com.example.partsorderapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PartsOrderApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PartsOrderApiApplication.class, args);
	}
}
