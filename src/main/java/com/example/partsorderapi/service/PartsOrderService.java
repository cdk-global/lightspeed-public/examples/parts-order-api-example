package com.example.partsorderapi.service;

import com.example.partsorderapi.dto.PurchaseOrder;
import com.example.partsorderapi.dto.PurchaseOrderLine;
import com.example.partsorderapi.dto.PurchaseOrdersSearchResult;
import com.example.partsorderapi.dto.ShippingOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.UUID;

@Service
public class PartsOrderService {

    private Logger log = LoggerFactory.getLogger(PartsOrderService.class);

    public PurchaseOrder importPurchaseOrder(String supplierDealerNumber, PurchaseOrder purchaseOrder) {
        //TODO set the supplier order number in the response back
        purchaseOrder.setSupplierPurchaseOrderNumber(UUID.randomUUID().toString());

        //TODO set a supplier status on each line
        for (PurchaseOrderLine line : purchaseOrder.getLines()) {
            line.setSupplierStatus("Ordered");
        }

        //TODO import the purchase order into your system

        //TODO ensure that the supplierPurchaseOrderID on the main purchase order object and the supplierPurchaseOrderLineID's
        //TODO  on each line come back as they were sent, otherwise the statuses will not show in Lightspeed correctly.
        assert StringUtils.hasText(purchaseOrder.getSupplierPurchaseOrderID());
        for (PurchaseOrderLine line : purchaseOrder.getLines()) {
            assert StringUtils.hasText(line.getSupplierPurchaseOrderLineID());
        }

        return purchaseOrder;
    }

    public PurchaseOrder getPurchaseOrder(String supplierDealerNumber, String purchaseOrderID) {
        //TODO retrieve the purchase order out of your system
        PurchaseOrder purchaseOrder = new PurchaseOrder();

        for (PurchaseOrderLine line : purchaseOrder.getLines()) {
            line.setSupplierStatus("Shipped"); //TODO set a status for each line on the order
        }

        //TODO ensure that the supplierPurchaseOrderID on the main purchase order object and the supplierPurchaseOrderLineID's
        //TODO  on each line are present or else the statuses will not show in Lightspeed correctly.
        assert StringUtils.hasText(purchaseOrder.getSupplierPurchaseOrderID());
        for (PurchaseOrderLine line : purchaseOrder.getLines()) {
            assert StringUtils.hasText(line.getSupplierPurchaseOrderLineID());
        }

        return purchaseOrder;
    }

    public PurchaseOrdersSearchResult searchPurchaseOrders(String supplierDealerNumber, LocalDate startDate, LocalDate endDate) {
        //TODO use the start date and end date to search for purchase orders in your system
        return new PurchaseOrdersSearchResult();
    }

    public ShippingOptions getShippingOptions(String supplierDealerNumber) {
        //TODO return the valid shipping options from your system
        return new ShippingOptions();
    }
}
